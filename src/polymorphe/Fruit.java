package polymorphe;

//Exo Java oriente objet de polymorphisme qui 
//permet de rechercher un fruit quelconque et 
//d'afficher son poids a l'ecran


public abstract class Fruit {
	protected int poids;
	public Fruit () {
		System.out.println("Creation d'un fruit");
	}
	public void affiche() {
		System.out.println("C'est un fruit");
	}
}